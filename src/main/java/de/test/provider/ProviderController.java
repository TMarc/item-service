package de.test.provider;

import de.test.provider.domain.BrandItem;
import io.micrometer.core.annotation.Timed;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * REST-Endpoints of the item-service
 */
@RestController
@Timed
@Log
public class ProviderController {

    private final ProviderService provider;

    @Autowired
    public ProviderController(ProviderService provider) {
        this.provider = provider;
    }

    @Timed
    @GetMapping(path = "/items/brands/all", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<BrandItem> getBrandItems() {
        log.info("requested brand items '/items/brands/all'");

        return this.provider.getBrands();
    }
}