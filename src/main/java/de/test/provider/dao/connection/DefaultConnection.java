package de.test.provider.dao.connection;

import lombok.extern.java.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Singleton class for the database connection
 */
@Log
public class DefaultConnection {
    /**
     * {@inheritDoc}
     */
    private static Connection connection = null;

    /**
     * @param driverName The jdbc driver
     * @param host       The host of the database
     * @param user       The username for the database access (can be null)
     * @param password   The password for the database access (can be null)
     * @return The {@link java.sql.Connection}
     * @throws SQLException see {@link java.sql.DriverManager#getConnection(String)} or {@link java.sql.DriverManager#getConnection(String, String, String)}
     **/
    public static Connection getConnection(String driverName, String host, String user, String password) throws SQLException {
        if (connection != null) {
            return connection;
        } else {
            try {
                Class.forName(driverName);
            } catch (ClassNotFoundException e) {
                log.severe(e.getMessage());

                return null;
            }

            if (user == null || password == null) {
                return DriverManager.getConnection(host);
            }

            return DriverManager.getConnection(host, user, password);
        }
    }
}