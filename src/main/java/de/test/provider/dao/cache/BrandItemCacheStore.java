package de.test.provider.dao.cache;

import de.test.provider.domain.BrandItem;
import de.test.provider.dao.BrandItemDAO;
import lombok.extern.java.Log;
import org.apache.ignite.cache.store.CacheStoreAdapter;
import org.apache.ignite.lang.IgniteBiInClosure;
import reactor.core.publisher.Flux;

import javax.cache.Cache;
import javax.cache.integration.CacheLoaderException;
import javax.cache.integration.CacheWriterException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * {@inheritDoc}
 */
@Log
public class BrandItemCacheStore extends CacheStoreAdapter<String, BrandItem> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadCache(IgniteBiInClosure<String, BrandItem> clo, Object... args) {
        super.loadCache(clo, args);

        log.info("load brand items into ignite cache");

        final AtomicInteger counter = new AtomicInteger();
        final Flux<BrandItem> items = new BrandItemDAO().getAllItems();

        items.doOnComplete(() -> log.info("loaded " + counter + " brand items into ignite cache")).subscribe(item -> {
            if (item != null) {
                counter.incrementAndGet();
                clo.apply(item.getBrand(), item);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrandItem load(String key) throws CacheLoaderException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(Cache.Entry<? extends String, ? extends BrandItem> entry) throws CacheWriterException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Object key) throws CacheWriterException {
    }
}
