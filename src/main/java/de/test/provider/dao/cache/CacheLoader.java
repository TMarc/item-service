package de.test.provider.dao.cache;

import de.test.provider.domain.BrandItem;
import de.test.utils.Identifiers;
import de.test.provider.dao.BrandItemDAO;
import de.test.provider.dao.ItemDAO;
import lombok.Getter;
import lombok.extern.java.Log;
import org.apache.ignite.Ignite;
import org.apache.ignite.configuration.CacheConfiguration;
import reactor.core.publisher.Flux;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Functionality for initial loading and reloading data into the ignite caches.
 */
@Log
@Getter
public class CacheLoader {
    /**
     * Boolean to check whether a reloading process is already running or not
     * so that the reloading process is not able to get triggered while another
     * reloading process is already / still running.
     */
    private boolean isReloading = false;
    /**
     * Since every single cache is getting reloaded in it's own thread this counter
     * is representing the amount of caches which are still reloading.
     */
    private AtomicInteger reloadCounter = new AtomicInteger(0);

    /**
     * Fill the ignite caches with data
     *
     * @param ignite                      The ignite
     * @param brandItemCacheConfiguration The brand items cache configuration
     */
    public void loadCache(Ignite ignite,
                          CacheConfiguration<String, BrandItem> brandItemCacheConfiguration) {

        log.info("load cache");

        new Thread(() -> loadItemsCache(ignite, brandItemCacheConfiguration)).start();
    }

    /**
     * Load the data for the given cache
     *
     * @param ignite             The ignite
     * @param cacheConfiguration The ignite cache configuration
     * @param <T>                Type of the key for the ignite cache
     * @param <V>                Type of the value for the ignite cache
     */
    private <T, V> void loadItemsCache(Ignite ignite, CacheConfiguration<T, V> cacheConfiguration) {
        log.info("load cache: " + cacheConfiguration.getName());

        ignite.getOrCreateCache(cacheConfiguration).loadCache(null);
    }

    /**
     * Reload the data for all caches
     *
     * @param ignite The ignite
     */
    public void reloadCache(Ignite ignite) {
        log.info("reload cache");

        this.isReloading = true;

        new Thread(() -> reloadBrandItemsCache(ignite)).start();
    }

    /**
     * Reload the data for the brand items cache
     *
     * @param ignite The ignite
     */
    private void reloadBrandItemsCache(Ignite ignite) {
        log.info("reload brand items cache");

        this.reloadItemsCacheDone(new BrandItemDAO()).subscribe(item -> ignite.getOrCreateCache(Identifiers.BRAND_IGNITE_ITEM_CACHE).put(item.getBrand(), item));
    }

    /**
     * This function is called after a cache is reloaded
     *
     * @param itemDAO the DAO for the cache
     * @param <T>     type of the key for the DAO
     * @param <V>     type of the value for the DAO and also the type of the flux
     * @return the flux of the given type
     */
    private <T, V> Flux<V> reloadItemsCacheDone(ItemDAO<T, V> itemDAO) {
        this.reloadCounter.incrementAndGet();

        return itemDAO.getAllItems().doOnComplete(() -> {
            this.reloadCounter.decrementAndGet();
            this.setIsReloading();
        });
    }

    /**
     * Set the {@code isReloading} to false when all caches are reloaded so that
     * the caches are ready to get reloaded again.
     */
    private void setIsReloading() {
        if (this.reloadCounter.get() == 0) {
            this.isReloading = false;
        }
    }
}