package de.test.provider.dao;

import de.test.configurations.DatabaseConfiguration;
import de.test.provider.dao.connection.DefaultConnection;
import de.test.provider.dao.transformers.BrandItemTransformer;
import de.test.provider.dao.utils.QueryExecutor;
import de.test.provider.domain.BrandItem;
import de.test.utils.helper.BeanUtil;
import lombok.extern.java.Log;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Data access object for {@link BrandItem}
 */
@Log
public class BrandItemDAO implements ItemDAO<String, BrandItem> {

    public static final String COLUMN_BRAND = "brand";

    /**
     * {@inheritDoc}
     */
    @Override
    public Mono<BrandItem> getItem(String key) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Flux<BrandItem> getAllItems() {
        final DatabaseConfiguration databaseConfiguration = BeanUtil.getBean(DatabaseConfiguration.class);
        final String query = String.format("select * from %s", databaseConfiguration.TABLE_NAME);

        try {
            final Connection connection = DefaultConnection.getConnection(databaseConfiguration.getDRIVER_NAME(), databaseConfiguration.getHOST(), databaseConfiguration.getUSER(), databaseConfiguration.getPASSWORD());

            return new QueryExecutor<BrandItem>().executeFluxQuery(connection, query, new BrandItemTransformer());
        } catch (SQLException e) {
            e.printStackTrace();

            return null;
        }
    }
}