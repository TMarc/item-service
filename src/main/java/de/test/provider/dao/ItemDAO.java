package de.test.provider.dao;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Interface for the data access objects.
 *
 * @param <T> type of the parameter
 * @param <V> type of the return value
 */
public interface ItemDAO<T, V> {

    /**
     * Get a single item of the given type.
     *
     * @param key The key of the given type
     * @return The result
     */
    Mono<V> getItem(T key);

    /**
     * Get all items of the given type.
     *
     * @return The results
     */
    Flux<V> getAllItems();
}
