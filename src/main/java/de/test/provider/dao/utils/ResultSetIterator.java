package de.test.provider.dao.utils;

import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Function;

/**
 * This adapter should transform {@link java.sql.ResultSet} into an {@link java.util.Iterator} of some domain object
 *
 * @param <T> type of the domain object
 */
public class ResultSetIterator<T> implements Iterable<T> {
    /**
     * The {@link java.sql.ResultSet} to transform
     */
    private ResultSet resultSet;
    /**
     * The {@link java.util.function.Function} used to transform the results of the {@link java.sql.ResultSet} into some domain object
     */
    private Function<ResultSet, T> transformer;

    /**
     * @param resultSet   The {@link java.sql.ResultSet} to transform
     * @param transformer The {@link java.util.function.Function} used to transform the results of the {@link java.sql.ResultSet} into some domain object
     */
    ResultSetIterator(ResultSet resultSet, Function<ResultSet, T> transformer) {
        this.resultSet = resultSet;
        this.transformer = transformer;
    }

    /**
     * {@inheritDoc}
     */
    @NotNull
    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            /**
             * {@inheritDoc}
             */
            @Override
            public boolean hasNext() {
                try {
                    return resultSet.next();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

            /**
             * {@inheritDoc}
             */
            @Override
            public T next() {
                /*
                  Attention: Do not listen to sonarqube and do not check for {@code hasNext()} here!
                  {@code hasNext()} is implemented to call {@code next()} of {@link java.sql.ResultSet}
                  which is putting the cursor on the next element and returning true if an element
                  exists or not. So if you would call it here you would skip one element every time
                  you call {@code next()} on this iterator.
                 */
                return transformer.apply(resultSet);
            }
        };
    }
}