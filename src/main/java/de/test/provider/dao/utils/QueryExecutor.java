package de.test.provider.dao.utils;

import lombok.extern.java.Log;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.function.Function;

/**
 * Provides generic functions to perform sql queries.
 *
 * @param <T> type of the queries return values
 */
@Log
public class QueryExecutor<T> {

    /**
     * Generic function for sql queries which expect a single result.
     *
     * @param query       The sql query
     * @param transformer The transformer which transforms a {@link java.sql.ResultSet} into the given type
     * @return The result of the given type for the given sql query
     */
    public Mono<T> executeQuery(Connection connection, String query, Function<ResultSet, T> transformer) {
        log.info("execute query: " + query);

        try {
            if (connection != null) {
                Statement statement = null;

                try {
                    statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(query);

                    ResultSetIterator<T> resultSetIterator = new ResultSetIterator<>(resultSet, transformer);
                    Iterator<T> iterator = resultSetIterator.iterator();
                    T result = null;

                    if (iterator.hasNext()) {
                        result = iterator.next();
                    }

                    if (result == null) {
                        return Mono.empty();
                    }

                    return Mono.just(result);
                } finally {
                    if (statement != null) {
                        statement.close();
                    }

                    connection.close();
                }
            }

            return null;
        } catch (SQLException e) {
            log.severe(e.getMessage());

            return null;
        }
    }

    /**
     * Generic function for sql queries which expect at least one result.
     *
     * @param query       The sql query
     * @param transformer The transformer which transforms a {@link java.sql.ResultSet} into the given type
     * @return The results of the given type for the given sql query
     */
    public Flux<T> executeFluxQuery(Connection connection, String query, Function<ResultSet, T> transformer) {
        log.info("execute flux query: " + query);

        if (connection != null) {
            // Nonblocking
            Mono<ResultSet> mono = Mono.fromCallable(() -> {
                Statement statement = connection.createStatement();

                try {
                    return statement.executeQuery(query);
                } catch (SQLException e) {
                    log.severe(e.getMessage());

                    return null;
                }
            });

            // Nonblocking
            return mono.flatMapMany(resultSet -> Flux.fromIterable(new ResultSetIterator<>(resultSet, transformer))).doOnComplete(() -> {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.severe(e.getMessage());
                }
            });
        }

        return null;
    }
}