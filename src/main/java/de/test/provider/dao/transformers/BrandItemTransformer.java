package de.test.provider.dao.transformers;

import de.test.provider.dao.BrandItemDAO;
import de.test.provider.dao.utils.ResultSetIterator;
import de.test.provider.domain.BrandItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

/**
 * Provides a {@link java.util.function.Function} to transform a {@link java.sql.ResultSet} into a {@link BrandItem}.
 *
 * @see ResultSetIterator
 */
public class BrandItemTransformer implements Function<ResultSet, BrandItem> {

    /**
     * {@inheritDoc}
     */
    @Override
    public BrandItem apply(ResultSet resultSet) {
        try {
            String brand = resultSet.getString(BrandItemDAO.COLUMN_BRAND);

            return new BrandItem(brand);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}