package de.test.provider;

import de.test.provider.dao.cache.CacheLoader;
import de.test.provider.domain.BrandItem;
import de.test.utils.Identifiers;
import lombok.extern.java.Log;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.Query;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import javax.cache.Cache;
import java.util.Iterator;

/**
 *
 */
@Service
@Log
public class ProviderService {

    private final Ignite ignite;
    private final CacheLoader cacheLoader;

    /**
     * Start the ignite cache and fill it with data
     *
     * @param brandItemCacheConfiguration cache configuration for brand items
     */
    @Autowired
    public ProviderService(@Autowired @Qualifier(Identifiers.BRAND_IGNITE_ITEM) CacheConfiguration<String, BrandItem> brandItemCacheConfiguration) {
        log.info("call service provider constructor");

        this.ignite = Ignition.getOrStart(new IgniteConfiguration());
        this.cacheLoader = new CacheLoader();
        this.cacheLoader.loadCache(this.ignite, brandItemCacheConfiguration);
    }

    Flux<BrandItem> getBrands() {
        log.info("get brands");

        return Flux.create(sink -> {
            log.info("lookup basket items cache");

            this.publishItemsFromCacheIterator(sink, Identifiers.BRAND_IGNITE_ITEM_CACHE);
        });
    }

    /**
     * Publish items from ignite cache using a given query into a given flux
     *
     * @param query      The sql query
     * @param sink       The flux sink
     * @param identifier Identifier of the ignite cache
     * @param <T>        Type of the key for the ignite cache
     * @param <V>        Type of the value for the ignite cache and also the type of the flux
     */
    private <T, V> void publishItemsFromCacheQuery(Query<Cache.Entry<T, V>> query, FluxSink<V> sink, String identifier) {
        try (QueryCursor<Cache.Entry<T, V>> cursor = this.ignite.getOrCreateCache(identifier).query(query)) {
            log.info("publish items from cache: " + identifier);

            for (Cache.Entry<T, V> entry : cursor) {
                sink.next(entry.getValue());
            }

            log.info("publishing items from cache done: " + identifier);

            cursor.close();

            sink.complete();
        }
    }

    /**
     * Publish items from ignite cache into a given flux
     *
     * @param sink       The flux sink
     * @param identifier Identifier of the ignite cache
     * @param <T>        Type of the flux
     */
    private <T> void publishItemsFromCacheIterator(FluxSink<T> sink, String identifier) {
        final Iterator<Cache.Entry<Object, Object>> iterator = this.ignite.getOrCreateCache(identifier).iterator();

        log.info("publish items from cache: " + identifier);

        while (iterator.hasNext()) {
            sink.next((T) iterator.next().getValue());
        }

        log.info("publishing items from cache done: " + identifier);

        sink.complete();
    }
}