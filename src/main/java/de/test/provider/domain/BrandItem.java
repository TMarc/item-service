package de.test.provider.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * POJO which represents a brand item (for the add product component)
 */
@Data
@AllArgsConstructor
public class BrandItem {
    private String brand;
}