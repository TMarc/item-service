package de.test.utils;

/**
 * Application wide identifiers
 */
public class Identifiers {
    public static final String BRAND_IGNITE_ITEM = "brand_item";
    public static final String BRAND_IGNITE_ITEM_CACHE = "brand_item_cache";
}