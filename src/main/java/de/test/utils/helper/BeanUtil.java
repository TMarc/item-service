package de.test.utils.helper;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Helper class to be able to inject spring beans into non component classes
 */
@Service
public class BeanUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    /**
     * The bean loader function
     *
     * @param beanClass The class of the bean which should get loaded
     * @param <T>       The type of the bean which should get loaded
     * @return The bean
     */
    public static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }
}