package de.test.configurations;

import de.test.utils.Identifiers;
import de.test.provider.dao.cache.BrandItemCacheStore;
import de.test.provider.domain.BrandItem;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.configuration.FactoryBuilder;

/**
 * Configuration beans for the ignite cache configurations.
 */
@Configuration
public class IgniteCacheConfiguration {

    /**
     * Create brand items cache configuration bean.
     *
     * @return the cache configuration
     */
    @Bean(name = Identifiers.BRAND_IGNITE_ITEM)
    @Qualifier(Identifiers.BRAND_IGNITE_ITEM)
    public CacheConfiguration<String, BrandItem> brandItemCacheConfiguration() {
        return this.getBrandItemCacheConfiguration();
    }

    /**
     * Cache configuration for the brand items
     *
     * @return the cache configuration
     */
    private CacheConfiguration<String, BrandItem> getBrandItemCacheConfiguration() {
        final CacheConfiguration<String, BrandItem> cacheConfiguration = new CacheConfiguration<>(Identifiers.BRAND_IGNITE_ITEM_CACHE);

        cacheConfiguration.setName(Identifiers.BRAND_IGNITE_ITEM_CACHE);
        cacheConfiguration.setIndexedTypes(String.class, BrandItem.class);
        cacheConfiguration.setAtomicityMode(CacheAtomicityMode.TRANSACTIONAL);
        cacheConfiguration.setCacheStoreFactory(FactoryBuilder.factoryOf(BrandItemCacheStore.class.getName()));
        cacheConfiguration.setReadThrough(true);
        cacheConfiguration.setWriteThrough(true);
        cacheConfiguration.setCopyOnRead(false);
        cacheConfiguration.setCacheMode(CacheMode.REPLICATED);
        cacheConfiguration.setOnheapCacheEnabled(true);

        return cacheConfiguration;
    }
}