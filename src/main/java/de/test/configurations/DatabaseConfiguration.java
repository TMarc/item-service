package de.test.configurations;

import lombok.Getter;
import lombok.extern.java.Log;

/**
 * The abstract configuration for the database connection and table names
 */
@Log
public abstract class DatabaseConfiguration {
    /**
     * The jdbc driver
     */
    @Getter
    String DRIVER_NAME = "";
    /**
     * The host of the database
     */
    @Getter
    String HOST = "";
    /**
     * The username for the database access (can be null)
     */
    @Getter
    String USER = "";
    /**
     * The password for the database access (can be null)
     */
    @Getter
    String PASSWORD = "";
    /**
     * Name of the table
     */
    public String TABLE_NAME = "";
}