package de.test.configurations;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Implementation for testing
 * {@inheritDoc}
 */
@Component
@Profile("test")
public class DatabaseTestConfiguration extends DatabaseConfiguration {

    public DatabaseTestConfiguration() {
        this.DRIVER_NAME = "org.apache.ignite.IgniteJdbcThinDriver";
        this.HOST = "jdbc:ignite:thin://127.0.0.1/";
        this.USER = null;
        this.PASSWORD = null;

        this.TABLE_NAME = "some_table";
    }
}