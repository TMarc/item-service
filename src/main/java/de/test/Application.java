package de.test;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.SpringVersion;

import static org.apache.ignite.IgniteSystemProperties.IGNITE_JETTY_LOG_NO_OVERRIDE;

@ComponentScan("de.test")
@SpringBootApplication
@EnableCaching
@Log
public class Application {

    public static void main(String[] args) throws Exception {
        System.setProperty(IGNITE_JETTY_LOG_NO_OVERRIDE, "true");

        log.info("Spring version: " + SpringVersion.getVersion());

        SpringApplication.run(Application.class, args);
    }
}