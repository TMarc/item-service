package de.test.provider.dao;

import de.test.provider.dao.database.Database;
import de.test.provider.domain.BrandItem;
import lombok.extern.java.Log;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Log
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class BrandItemDAOTest {

    private boolean init = false;

    @Before
    public void init() {
        if (!this.init) {
            Ignition.getOrStart(new IgniteConfiguration());

            try {
                Database database = new Database();
                database.loadItemsForBrandItemsTest();
            } catch (SQLException e) {
                log.severe(e.getMessage());
            }

            this.init = true;
        }
    }

    @AfterClass
    public static void deInit() {
        Ignition.stopAll(true);
    }

    @Test
    public void getItem() {
    }

    @Test
    public void getAllItems() {
        log.info("test get all items");

        Map<String, BrandItem> expected = new HashMap<>();
        expected.put("SAMSUNG", new BrandItem("SAMSUNG"));
        expected.put("LG", new BrandItem("LG"));
        expected.put("BENQ", new BrandItem("BENQ"));
        expected.put("ASUS", new BrandItem("ASUS"));

        Map<String, BrandItem> result = new BrandItemDAO().getAllItems().collectMap(
                BrandItem::getBrand,
                item -> item).block();

        Assert.assertEquals(expected, result);
    }
}