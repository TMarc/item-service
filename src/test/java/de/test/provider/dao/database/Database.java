package de.test.provider.dao.database;

import de.test.configurations.DatabaseConfiguration;
import de.test.provider.dao.connection.DefaultConnection;
import de.test.utils.helper.BeanUtil;
import lombok.extern.java.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@Log
public class Database {
    private static final String COLUMN_ITEM_ID = "item_id";
    private static final String COLUMN_BRAND = "brand";

    private DatabaseConfiguration databaseConfiguration;
    private Connection connection;
    private PreparedStatement preparedStatement;

    public Database() throws SQLException {
        log.info("connect to database");

        this.databaseConfiguration = BeanUtil.getBean(DatabaseConfiguration.class);
        this.connection = DefaultConnection.getConnection(this.databaseConfiguration.getDRIVER_NAME(), this.databaseConfiguration.getHOST(), this.databaseConfiguration.getUSER(), this.databaseConfiguration.getPASSWORD());

        this.init();

        if (this.connection != null) {
            this.preparedStatement = this.connection.prepareStatement("INSERT INTO " + this.databaseConfiguration.TABLE_NAME + "(" + COLUMN_ITEM_ID + ", " + COLUMN_BRAND + ") VALUES(?, ?)");
        }
    }

    private void init() {
        log.info("create tables");

        Statement statement = null;

        try {
            if (this.connection != null) {
                statement = this.connection.createStatement();
                statement.executeUpdate("CREATE TABLE IF NOT EXISTS " + this.databaseConfiguration.TABLE_NAME + "(" + COLUMN_ITEM_ID + " VARCHAR PRIMARY KEY, " + COLUMN_BRAND + " VARCHAR) WITH \"template=replicated\"");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void deInit() {
        if (this.connection != null) {
            try {
                this.connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (this.preparedStatement != null) {
            try {
                this.preparedStatement.close();
            } catch (SQLException e) {
                log.warning(e.getMessage());
            }
        }
    }

    public void loadItemsForBrandItemsTest() {
        log.info("load items for brand items test");

        try {
            if (this.connection != null) {
                this.preparedStatement.setString(1, "1");
                this.preparedStatement.setString(2, "SAMSUNG");
                this.preparedStatement.executeUpdate();

                this.preparedStatement.setString(1, "2");
                this.preparedStatement.setString(2, "LG");
                this.preparedStatement.executeUpdate();

                this.preparedStatement.setString(1, "3");
                this.preparedStatement.setString(2, "BENQ");
                this.preparedStatement.executeUpdate();

                this.preparedStatement.setString(1, "4");
                this.preparedStatement.setString(2, "ASUS");
                this.preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            log.warning(e.getMessage());
        } finally {
            this.deInit();
        }
    }
}