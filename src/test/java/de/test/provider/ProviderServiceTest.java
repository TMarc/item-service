package de.test.provider;

import de.test.provider.domain.BrandItem;
import de.test.utils.Identifiers;
import lombok.extern.java.Log;
import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@Log
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class ProviderServiceTest {

    @Autowired
    private ProviderService provider;

    private Ignite ignite;

    private boolean init = false;

    @Before
    public void init() {
        if (!this.init) {
            this.ignite = Ignition.getOrStart(new IgniteConfiguration());
            this.init = true;
        }
    }

    @AfterClass
    public static void deInit() {
        Ignition.stopAll(true);
    }

    @Test
    public void reloadCache() {
        log.info("test reload cache");
    }

    @Test
    public void getBrands() {
        log.info("test get brands");

        Map<String, BrandItem> expected = new HashMap<>();
        expected.put("SAMSUNG", new BrandItem("SAMSUNG"));
        expected.put("LG", new BrandItem("LG"));
        expected.put("ASUS", new BrandItem("ASUS"));

        this.ignite.getOrCreateCache(Identifiers.BRAND_IGNITE_ITEM_CACHE).removeAll();
        this.ignite.getOrCreateCache(Identifiers.BRAND_IGNITE_ITEM_CACHE).putAll(expected);

        Map<String, BrandItem> result = this.provider.getBrands().collectMap(
                BrandItem::getBrand,
                item -> item).block();

        Assert.assertEquals(expected, result);
    }
}