package de.test.provider;

import de.test.provider.domain.BrandItem;
import lombok.extern.java.Log;
import org.apache.ignite.Ignition;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;

import java.util.Objects;

@Log
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@WebFluxTest(ProviderController.class)
public class ProviderControllerTest {

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private ProviderService provider;

    @AfterClass
    public static void deInit() {
        Ignition.stopAll(true);
    }

    @Test
    public void getBrandItems() {
        log.info("test get brand items: '/items/brands/all'");

        Flux<BrandItem> result = Flux.create(sink -> {
            sink.next(new BrandItem("SAMSUNG"));
            sink.next(new BrandItem("LG"));
            sink.next(new BrandItem("ASUS"));
            sink.next(new BrandItem("BENQ"));
            sink.complete();
        });

        Mockito.when(this.provider.getBrands()).thenReturn(result);

        this.webClient.get().uri("/items/brands/all")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(BrandItem.class).isEqualTo(Objects.requireNonNull(result.collectList().block()));
    }
}